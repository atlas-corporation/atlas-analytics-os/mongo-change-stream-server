/// <reference lib="dom" />
interface ObjectId {
	[Symbol(id)]: Uint8Array;
}

interface Timestamp {
	low: number;
	high: number;
	unsigned: boolean;
}

interface Namespace {
	db: string;
	coll: string;
}

interface ChangeStreamMessage {
	_id: {
		_data: string;
	};
	operationType: string;
	clusterTime: Timestamp;
	fullDocument: FullDocument;
	ns: Namespace;
	documentKey: {
		_id: ObjectId;
	};
}

interface Position {
	x: number;
	y: number;
	z: number;
}

interface SceneInitData {
	platform: string;
	startTime: number;
	endTime: number;
	analyticsVersion: string;
	parcels: string[];
	tags: string[];
	pollingInterval: number;
}

interface FullDocument {
	_id: string;
	player: string;
	guest: boolean;
	playerPosition: Position;
	playerRotation: Position;
	realm: string;
	sceneName: string;
	sceneBranch: string;
	timestamp: number;
	eventName: string;
	data: {
		playerName: string;
		sceneInitData: SceneInitData;
	};
	auth: string;
	userAgent: string;
	ip: string;
	sceneId: string;
	parcel: number[];
}
