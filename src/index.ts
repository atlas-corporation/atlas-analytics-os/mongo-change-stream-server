import dotenv from "dotenv";
import { MongoClient } from "mongodb";

dotenv.config();

const debug = process.env.DEBUG === "true" ? true : false;

const uri = process.env.DB_URI!;

const client = new MongoClient(uri);

const main = async () => {
	try {
		await client.connect();
		console.log("Connected to MongoDB");
		await watchCollection();
	} catch (err) {
		console.error("Failed to connect to MongoDB", err);
		process.exit(1);
	}
};

const watchCollection = async () => {
	const db = client.db(process.env.DB_NAME_WATCH!);
	const collection = db.collection(process.env.DB_COLLECTION_NAME_WATCH!);

	const condition = {
		eventName: "load-timer",
	};

	const pipeline = [
		{
			$match: {
				operationType: "insert",
				"fullDocument.eventName": "load-timer",
			},
		},
	];

	console.log(
		`Watching Change Stream - ${db.databaseName}:${collection.collectionName}`
	);
	const changeStream = collection.watch(pipeline, {
		fullDocument: "updateLookup",
	});

	changeStream.on("change", (next: ChangeStreamMessage) => {
		if (debug) console.log("Load time document detected", next);
		eventHandeler(next);
	});
};

const eventHandeler = async (changeStreamMessage: ChangeStreamMessage) => {
	if (debug) console.dir(changeStreamMessage, { depth: "full" });

	const db = client.db(process.env.DB_NAME_CACHE!);
	const cacheCollection = db.collection(
		process.env.DB_COLLECTION_NAME_CACHE!
	);

	// Extract relevant data from the document
	const sceneName = changeStreamMessage.fullDocument.sceneName;
	const sceneId = changeStreamMessage.fullDocument.sceneId;
	const branch = changeStreamMessage.fullDocument.sceneBranch;
	const parcels = changeStreamMessage.fullDocument.data.sceneInitData.parcels;
	const pollingRate =
		changeStreamMessage.fullDocument.data.sceneInitData.pollingInterval;
	const analyticsVersion =
		changeStreamMessage.fullDocument.data.sceneInitData.analyticsVersion;
	const tags = changeStreamMessage.fullDocument.data.sceneInitData.tags;

	// Filter the tags array to keep only those starting with "atlas:" and extract the Ethereum addresses
	const approvedAccounts = tags
		.filter((tag) => tag.startsWith("atlas:"))
		.map((tag) => tag.replace("atlas:", ""));

	// Construct a unique ID based on the scene name and parcel list
	const uniqueId = `${sceneName}-${JSON.stringify(parcels)}`;

	if (debug)
		console.log({
			uniqueId,
			sceneName,
			parcels,
			pollingRate,
			analyticsVersion,
			tags,
			approvedAccounts,
		});

	// Create a filter to find the cache document by the unique ID
	const filter = {
		sceneId: uniqueId,
	};

	const timestamp = Date.now();

	// Prepare the update operation
	const update = {
		$set: {
			pollingRate: pollingRate,
			analyticsVersion: analyticsVersion,
			approvedAccounts: approvedAccounts,
			updated: timestamp,
		},
		$addToSet: {
			sceneHashes: sceneId,
			branches: branch,
		},
		$setOnInsert: {
			sceneName: sceneName,
			parcels: parcels,
			created: timestamp,
		},
	};

	if (debug) console.dir({ filter, update }, { depth: "full" });

	// Upsert option to insert a new document if it doesn't exist
	const options = {
		upsert: true,
	};

	const result = await cacheCollection.updateOne(filter, update, options);

	if (result.upsertedCount) {
		console.log("Created new cache entry:", result.upsertedId);
	} else if (result.modifiedCount) {
		console.log("Updated existing cache entry");
	} else {
		console.log("No cache entry updated");
	}
};

main();
