module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ["eslint:recommended", "plugin:@typescript-eslint/recommended"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    project: "tsconfig.json",
  },
  plugins: ["@typescript-eslint"],
  ignorePatterns: ["dist/**", ".eslintrc.js"],
  rules: {
    "no-useless-escape": 0,
    "@typescript-eslint/no-non-null-assertion": 0,
    "int@typescript-eslint/no-explicit-any": 0,
    "@typescript-eslint/no-explicit-any": 0,
    "@typescript-eslint/ban-ts-comment": 0,
  },
};
